from pygame import init
from pygame.event import get
from pygame.display import flip
from pygame.locals import K_ESCAPE, KEYDOWN, QUIT, K_SPACE
from collections import deque
from constants import *
from pixel_class import Pixel
from numpy.random import randint, choice
from numpy import argmin
from time import sleep

6

def Snake():
    def direction_getter(my_snake, goal, direction):
        pos = my_snake[0]
        my_list = []
        for dir in direction_list:
            my_list.append(abs(pos[0]+dir[0]-goal[0]) + abs(pos[1]+dir[1]-goal[1]))
        index = argmin(my_list)
        if direction_list[index] == (-direction[0], -direction[1]):
            # pick next best
            my_list[index] = WIDTH+HEIGHT
            index = argmin(my_list)
        return direction_list[index]

    def get_goal_space(my_snake):
        locs = []
        for i in range(WIDTH):
            for j in range(HEIGHT):
                if (i,j) not in my_snake:
                    locs.append((i,j))
        return locs

    def advance(my_snake, goal, direction, grid):
        new_head_loc = (my_snake[0][0]+direction[0], my_snake[0][1]+direction[1])
        if 0<=new_head_loc[0]<WIDTH and 0<=new_head_loc[1]<HEIGHT and new_head_loc not in my_snake:
            if new_head_loc == goal:
                my_snake.appendleft(new_head_loc)
                grid[my_snake[0][0]][my_snake[0][1]].change_state(2)
                grid[my_snake[1][0]][my_snake[1][1]].change_state(1)
                if len(my_snake) > 0.85*WIDTH*HEIGHT:
                    locs = get_goal_space(my_snake)
                    while goal not in my_snake:
                        goal = choice(locs)
                else:
                    goal = (randint(0, WIDTH), randint(HEIGHT))
                    while goal in my_snake:
                        goal = (randint(0, WIDTH), randint(HEIGHT))
                    grid[goal[0]][goal[1]].change_state(3)
            else:
                my_snake.appendleft(new_head_loc)
                grid[my_snake[0][0]][my_snake[0][1]].change_state(2)
                grid[my_snake[1][0]][my_snake[1][1]].change_state(1)
                grid[my_snake[-1][0]][my_snake[-1][1]].change_state(0)
                my_snake.pop()

        else:
            return my_snake, goal, direction, grid, True

        return my_snake, goal, direction, grid, False

    # Initialize pygame
    init()

    # create the grid
    grid = [[Pixel(i,j) for j in range(HEIGHT)] for i in range(WIDTH)]

    my_snake = deque()
    my_snake.append((randint(0, WIDTH), randint(HEIGHT)))
    grid[my_snake[0][0]][my_snake[0][1]].change_state(2)

    goal = (randint(0, WIDTH), randint(0, HEIGHT))
    while goal in my_snake:
        goal = (randint(0, WIDTH), randint(0, HEIGHT))
    grid[goal[0]][goal[1]].change_state(3)

    direction_list = [(1,0), (-1,0), (0,1), (0,-1)]
    temp_direction = direction_list[randint(0,4)]
    # guarantee that there is at least 5 moves prior to losing to begin with
    while not 0<=my_snake[0][0]+5*temp_direction[0]<WIDTH and 0<=my_snake[0][1]+5*temp_direction[1]<HEIGHT:
        temp_direction = direction_list[randint(0,4)]
    direction = temp_direction

    running = True
    speed = 1
    while running:
        if len(my_snake) == WIDTH*HEIGHT:
            print('You won!')
            return

        direction = direction_getter(my_snake, goal, direction)

        events = get()
        for event in events:
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    return
                elif event.key == K_SPACE:
                    speed = (speed+1) % 5

            elif event.type == QUIT:
                return

        my_snake, goal, direction, grid, lost = advance(my_snake, goal, direction, grid)
        # print(my_snake)
        # print(grid[my_snake[0][0]][my_snake[0][1]].color)

        if lost:
            print("You lose!")
            print("Score: {}".format(len(my_snake)))
            return

        # draw what needs to be drawn
        screen.fill(BLACK)
        for i in range(WIDTH):
            for j in range(HEIGHT):
                grid[i][j].show()
        flip()

        if speed > 0:
            sleep((speed % 5)**-2 * hz)


if __name__ == '__main__':
    Snake()
