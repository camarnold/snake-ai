from pygame import init
from pygame.event import get
from pygame.display import flip
from pygame.locals import K_ESCAPE, KEYDOWN, QUIT, K_UP, K_w, K_DOWN, K_s, K_LEFT, K_a, K_RIGHT, K_d
from collections import deque
from constants import *
from pixel_class import Pixel
from numpy.random import randint
from numpy import argmin, infty, array
from time import sleep


# https://www.geeksforgeeks.org/hamiltonian-cycle-backtracking-6/
class Graph():
    def __init__(self, WIDTH, HEIGHT):
        self.width = WIDTH
        self.height = HEIGHT
        self.adjacency_list = [[[] for j in range(self.height)] for i in range(self.width)]
        for i in range(self.width):
            for j in range(self.height):
                if 0<=i+1<self.width:
                    self.adjacency_list[i][j].append((i+1, j))
                if 0<=i-1<self.width:
                    self.adjacency_list[i][j].append((i-1, j))
                if 0<=j+1<self.height:
                    self.adjacency_list[i][j].append((i, j+1))
                if 0<=j-1<self.height:
                    self.adjacency_list[i][j].append((i, j-1))

    # return whether it's safe to add vertex/position
    def is_safe(self, vertex, path):
        # is vertex adjacent to the most recent vertex/position in path?
        # breakpoint()
        if vertex in self.adjacency_list[path[-1][0]][path[-1][1]]:
            return False
        # is vertex already in path?
        if vertex in path:
            return False
        return True

    # recursive function to help solve the problem
    def ham_cycle_util(self, path):
        # if all vertices are in the path:
        if len(path) == self.width*self.height:
            if path[0] in self.adjacency_list[path[-1][0]][path[-1][1]]:
                path.append(path[0])
                return path
            else:
                return False

        # try different vertices as a next candidate in the Hamiltonian cycle
        for i in range(self.width):
            for j in range(self.height):
                if self.is_safe((i,j), path):
                    path.append((i,j))

                    if self.ham_cycle_util(path):
                        return True

                    else:
                        path.pop()

        return False

    # main solve function
    def ham_cycle(self, start_pos, start_dir):
        path = deque([start_pos])
        path.append((start_pos[0] + start_dir[0], start_pos[1] + start_dir[1]))
        path = self.ham_cycle_util(path)
        if path == False:
            print('There is no Hamiltonian Path.')
            return []
        return path

    # function to get the directions from path
    def direction_getter(self, path):
        direction_list = deque([])
        for i in range(len(path)-1):
            direction_list.append((path[i+1][0]-path[i][0], path[i+1][1]-path[i][1]))

        return direction_list


def Snake():

    def get_goal_space(my_snake):
        locs = []
        for i in range(WIDTH):
            for j in range(HEIGHT):
                if (i,j) not in my_snake:
                    locs.append((i,j))
        return locs

    def advance(my_snake, goal, direction, grid):
        new_head_loc = (my_snake[0][0]+direction[0], my_snake[0][1]+direction[1])
        if 0<=new_head_loc[0]<WIDTH and 0<=new_head_loc[1]<HEIGHT and new_head_loc not in my_snake:
            if new_head_loc == goal:
                my_snake.appendleft(new_head_loc)
                grid[my_snake[0][0]][my_snake[0][1]].change_state(2)
                grid[my_snake[1][0]][my_snake[1][1]].change_state(1)
                if len(my_snake) > 0.85*WIDTH*HEIGHT:
                    locs = get_goal_space(my_snake)
                    while goal not in my_snake:
                        goal = choice(locs)
                else:
                    goal = (randint(0, WIDTH), randint(HEIGHT))
                    while goal in my_snake:
                        goal = (randint(0, WIDTH), randint(HEIGHT))
                    grid[goal[0]][goal[1]].change_state(3)
            else:
                my_snake.appendleft(new_head_loc)
                grid[my_snake[0][0]][my_snake[0][1]].change_state(2)
                grid[my_snake[1][0]][my_snake[1][1]].change_state(1)
                grid[my_snake[-1][0]][my_snake[-1][1]].change_state(0)
                my_snake.pop()

        else:
            return my_snake, goal, direction, grid, True

        return my_snake, goal, direction, grid, False

    # Initialize pygame
    init()

    # create the grid
    grid = [[Pixel(i,j) for j in range(HEIGHT)] for i in range(WIDTH)]

    my_snake = deque()
    my_snake.append((randint(0, WIDTH), randint(HEIGHT)))
    grid[my_snake[0][0]][my_snake[0][1]].change_state(2)

    goal = (randint(0, WIDTH), randint(0, HEIGHT))
    while goal in my_snake:
        goal = (randint(0, WIDTH), randint(0, HEIGHT))
    grid[goal[0]][goal[1]].change_state(3)

    direction_list = [(1,0), (-1,0), (0,1), (0,-1)]
    temp_direction = direction_list[randint(0,4)]
    # guarantee that there is at least 5 moves prior to losing to begin with
    while not 0<=my_snake[0][0]+5*temp_direction[0]<WIDTH and 0<=my_snake[0][1]+5*temp_direction[1]<HEIGHT:
        temp_direction = direction_list[randint(0,4)]
    direction = temp_direction

    # graph
    my_graph = Graph(WIDTH, HEIGHT)
    path = my_graph.ham_cycle(my_snake[0], direction)
    direction_list = my_graph.direction_getter(path)
    direction = direction_list.popleft()
    direction_list.append(direction)

    running = True
    while running:
        if len(my_snake) == WIDTH*HEIGHT:
            print('You won!')
            return

        direction = direction_list.popleft()
        direction_list.append(direction)

        events = get()
        for event in events:
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    return
            elif event.type == QUIT:
                return

        my_snake, goal, direction, grid, lost = advance(my_snake, goal, direction, grid)
        # print(my_snake)
        # print(grid[my_snake[0][0]][my_snake[0][1]].color)

        if lost:
            print("You lose!")
            print("Score: {}".format(len(my_snake)))
            return

        # draw what needs to be drawn
        screen.fill(BLACK)
        for i in range(WIDTH):
            for j in range(HEIGHT):
                grid[i][j].show()
        flip()

        sleep(hz)


if __name__ == '__main__':

    Snake()
