from pygame import init
from pygame.event import get
from pygame.display import flip
from pygame.locals import K_ESCAPE, KEYDOWN, QUIT, K_SPACE
from collections import deque
from queue import PriorityQueue
from constants import *
from pixel_class import Pixel
from numpy.random import randint, choice
from numpy import argmin, infty, array
from time import sleep


def A_Star(real_grid, BEGINNING, END, deque_snake):
    def reconstruct_path(camefrom, cur):
        if cur != BEGINNING:
            total_path = [cur]
        else:
            total_path = []
        while camefrom[cur[0]][cur[1]] != None:
            cur = camefrom[cur[0]][cur[1]]
            if cur != END and cur != BEGINNING:
                total_path.insert(0, cur)
        return total_path

    def neighbor_getter(pos):
        legal_moves = []
        for i,j in MOVES:
            # if we haven't visited this state (and it exists)
            if 0<=pos[0]+i<WIDTH and 0<=pos[1]+j<HEIGHT:
                # if grid[pos[0]+i][pos[1]+j].state != -1 and [pos[0]+i, pos[1]+j] != BEGINNING:
                if grid[pos[0]+i][pos[1]+j].state in [0, 1, 2, 3] and [pos[0]+i, pos[1]+j] not in reconstruct_path(came_from, pos):
                    legal_moves.append([pos[0]+i, pos[1]+j])
        return legal_moves

    # function to approximate distance to goal
    h = lambda cur_pos: abs(cur_pos[0]-END[0]) + abs(cur_pos[1]-END[1])
    # h = lambda cur_pos: (cur_pos[0]-END[0])**2 + (cur_pos[1]-END[1])**2

    # copy grid and change into the A_Star format
    grid = real_grid.copy()
    for i in range(WIDTH):
        for j in range(HEIGHT):
            if grid[i][j].state == 1:
                if abs(i-BEGINNING[0]) + abs(j-BEGINNING[1]) > deque_snake.index((i,j)):
                    grid[i][j].state = 0
                else:
                    grid[i][j].state = -1
            elif grid[i][j].state == 2:
                grid[i][j].state = -1
            elif grid[i][j].state == 3:
                grid[i][j].state = 1

    # keep track of when we update nodes
    count = 0

    # PriorityQueue is an efficient data structure for this and will hold
    # all nodes that are being used
    open_set = PriorityQueue()
    open_set.put((0, -count, BEGINNING))

    # stores whether point [i,j] is currently in the PriorityQueue, which can't
    # efficiently do this
    in_open_set = [[False for j in range(HEIGHT)] for i in range(WIDTH)]
    in_open_set[BEGINNING[0]][BEGINNING[1]] = True

    # Stores the previous node in the best route to point [i,j]
    came_from = [[None for j in range(HEIGHT)] for i in range(WIDTH)]

    # g_score contains the best current cost to get to point [i,j]
    g_score = [[infty for j in range(HEIGHT)] for i in range(WIDTH)]
    g_score[BEGINNING[0]][BEGINNING[1]] = 0

    # f_score contains the best current estimate to get from beginning to end
    # when going through point [i,j]
    f_score = [[0 for j in range(HEIGHT)] for i in range(WIDTH)]
    f_score[BEGINNING[0]][BEGINNING[1]] = h(BEGINNING)

    MOVES = [[0,1], [0,-1], [1,0], [-1,0]]

    running = True
    solving = True
    solved = False
    while running:
        if solving:
            if open_set.empty():
                print('There is no path from beginning to end.')
                return []
            current = open_set.get()[2]
            in_open_set[current[0]][current[1]] = False

            if current == END:
                move_list = reconstruct_path(came_from, current)
                return move_list

            neighbors = neighbor_getter(current)
            for neighbor in neighbors:
            	temp_g_score = g_score[current[0]][current[1]] + 1
            	if temp_g_score < g_score[neighbor[0]][neighbor[1]]:
            		came_from[neighbor[0]][neighbor[1]] = current
            		g_score[neighbor[0]][neighbor[1]] = temp_g_score
            		f_score[neighbor[0]][neighbor[1]] = -(temp_g_score + h(neighbor))
            		if not in_open_set[neighbor[0]][neighbor[1]]:
            			count += 1
            			open_set.put((f_score[neighbor[0]][neighbor[1]], -count, neighbor))
            			in_open_set[neighbor[0]][neighbor[1]] = True
            			grid[neighbor[0]][neighbor[1]].state = 3

            if current not in [BEGINNING, END]:
                grid[current[0]][current[1]].state = 2

        elif solved:
            return move_list

        events = get()
        for event in events:
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    return []
            elif event.type == QUIT:
                return []

    return []


def Snake():
    def direction_getter(my_snake, goal, direction):
        pos = my_snake[0]
        my_list = []
        for dir in direction_list:
            my_list.append(abs(pos[0]+dir[0]-goal[0]) + abs(pos[1]+dir[1]-goal[1]))
        index = argmin(my_list)
        if direction_list[index] == (-direction[0], -direction[1]):
            # pick next best
            my_list[index] = WIDTH+HEIGHT
            index = argmin(my_list)
        return direction_list[index]

    def get_goal_space(my_snake):
        locs = []
        for i in range(WIDTH):
            for j in range(HEIGHT):
                if (i,j) not in my_snake:
                    locs.append((i,j))
        return locs

    def advance(my_snake, goal, direction, grid):
        new_head_loc = (my_snake[0][0]+direction[0], my_snake[0][1]+direction[1])
        if 0<=new_head_loc[0]<WIDTH and 0<=new_head_loc[1]<HEIGHT and new_head_loc not in my_snake:
            if new_head_loc == goal:
                my_snake.appendleft(new_head_loc)
                grid[my_snake[0][0]][my_snake[0][1]].change_state(2)
                grid[my_snake[1][0]][my_snake[1][1]].change_state(1)
                if len(my_snake) > 0.85*WIDTH*HEIGHT:
                    locs = get_goal_space(my_snake)
                    while goal not in my_snake:
                        goal = choice(locs)
                else:
                    goal = (randint(0, WIDTH), randint(HEIGHT))
                    while goal in my_snake:
                        goal = (randint(0, WIDTH), randint(HEIGHT))
                    grid[goal[0]][goal[1]].change_state(3)
            else:
                my_snake.appendleft(new_head_loc)
                grid[my_snake[0][0]][my_snake[0][1]].change_state(2)
                grid[my_snake[1][0]][my_snake[1][1]].change_state(1)
                grid[my_snake[-1][0]][my_snake[-1][1]].change_state(0)
                my_snake.pop()

        else:
            return my_snake, goal, direction, grid, True

        return my_snake, goal, direction, grid, False

    # Initialize pygame
    init()

    # create the grid
    grid = [[Pixel(i,j) for j in range(HEIGHT)] for i in range(WIDTH)]

    my_snake = deque()
    my_snake.append((randint(0, WIDTH), randint(HEIGHT)))
    grid[my_snake[0][0]][my_snake[0][1]].change_state(2)

    goal = (randint(0, WIDTH), randint(0, HEIGHT))
    while goal in my_snake:
        goal = (randint(0, WIDTH), randint(0, HEIGHT))
    grid[goal[0]][goal[1]].change_state(3)

    direction_list = [(1,0), (-1,0), (0,1), (0,-1)]
    temp_direction = direction_list[randint(0,4)]
    # guarantee that there is at least 5 moves prior to losing to begin with
    while not 0<=my_snake[0][0]+5*temp_direction[0]<WIDTH and 0<=my_snake[0][1]+5*temp_direction[1]<HEIGHT:
        temp_direction = direction_list[randint(0,4)]
    direction = temp_direction

    running = True
    move_list = []
    screwed = False
    speed = 1
    while running:
        if len(my_snake) == WIDTH*HEIGHT:
            print('You won!')
            return

        if len(move_list) == 0 and not screwed:
            move_list = deque(A_Star(grid.copy(), list(my_snake[0]), list(goal), my_snake.copy()))
            for i in range(WIDTH):
                for j in range(HEIGHT):
                    if (i,j) == goal:
                        grid[i][j].change_state(3)
                    elif (i,j) == my_snake[0]:
                        grid[i][j].change_state(2)
                    elif (i,j) in my_snake:
                        grid[i][j].change_state(1)
                    else:
                        grid[i][j].change_state(0)

            if len(move_list) == 0:
                screwed = True
        if not screwed:
            new_pos = move_list.popleft()
            direction = (new_pos[0]-my_snake[0][0], new_pos[1]-my_snake[0][1])

        events = get()
        for event in events:
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    return
                elif event.key == K_SPACE:
                    speed = (speed+1) % 5
            elif event.type == QUIT:
                return

        my_snake, goal, direction, grid, lost = advance(my_snake, goal, direction, grid)
        # print(my_snake)
        # print(grid[my_snake[0][0]][my_snake[0][1]].color)

        if lost:
            print("You lose!")
            print("Score: {}".format(len(my_snake)))
            return

        # draw what needs to be drawn
        screen.fill(BLACK)
        for i in range(WIDTH):
            for j in range(HEIGHT):
                grid[i][j].show()
        flip()

        if speed > 0:
            sleep((speed % 5)**-2 * hz)


if __name__ == '__main__':
    Snake()
