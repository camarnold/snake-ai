# snake-ai
To play the game yourself, run main.py

To watch an AI play the game, run the corresponding policy file.

# Descriptions
In each policy file, ESC will quit the simulation and SPACE will toggle the speed from 1x - 16x, as well as quickly as possible.

random_policy.py moves completely randomly

closest_first_policy.py will always move the head of the snake to the goal along the quickest path possible regardless of the locations of the tail.

a_star_policy.py will move the snake to the along the quickest path utilizing [A* Path Planning](https://en.wikipedia.org/wiki/A*_search_algorithm) that takes into account the locations of the tail.

longest_path_a_star_policy.py will find a very long path (not necessarily the longest possible path) to get the head to the goal, and again takes into account the locations of the tail.

hamiltonian_path_policy will find a [Hamiltonian Path](https://en.wikipedia.org/wiki/Hamiltonian_path) (assuming WIDTH >= 2, HEIGHT >= 4, and HEIGHT % 2 == 0) and follow along that path indefinitely until the game has been won.  One could easily implement an algorithm to attempt to find a Hamiltonian Path for a wider range of restrictions.
