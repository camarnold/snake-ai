from pygame import init
from pygame.event import get
from pygame.display import flip
from pygame.locals import K_ESCAPE, KEYDOWN, QUIT, K_SPACE
from collections import deque
from constants import *
from pixel_class import Pixel
from numpy.random import randint
from numpy import argmin, infty, array
from time import sleep


def Snake():

    def get_goal_space(my_snake):
        locs = []
        for i in range(WIDTH):
            for j in range(HEIGHT):
                if grid[i][j].state == 0:
                    locs.append((i,j))
        return locs

    def advance(my_snake, goal, direction, grid):
        new_head_loc = (my_snake[0][0]+direction[0], my_snake[0][1]+direction[1])
        if 0<=new_head_loc[0]<WIDTH and 0<=new_head_loc[1]<HEIGHT and new_head_loc not in my_snake:
            if new_head_loc == goal:
                my_snake.appendleft(new_head_loc)
                grid[my_snake[0][0]][my_snake[0][1]].change_state(2)
                grid[my_snake[1][0]][my_snake[1][1]].change_state(1)
                if len(my_snake) > 0.9*WIDTH*HEIGHT:
                    locs = get_goal_space(grid)
                    if len(locs) > 0:
                        goal = locs[randint(0, len(locs))]
                        grid[goal[0]][goal[1]].change_state(3)
                else:
                    goal = (randint(0, WIDTH), randint(HEIGHT))
                    while goal in my_snake:
                        goal = (randint(0, WIDTH), randint(HEIGHT))
                    grid[goal[0]][goal[1]].change_state(3)
            else:
                my_snake.appendleft(new_head_loc)
                grid[my_snake[0][0]][my_snake[0][1]].change_state(2)
                grid[my_snake[1][0]][my_snake[1][1]].change_state(1)
                grid[my_snake[-1][0]][my_snake[-1][1]].change_state(0)
                my_snake.pop()

        else:
            return my_snake, goal, direction, grid, True

        return my_snake, goal, direction, grid, False

    def move_and_add(pos, dir, dir_list):
        dir_list.append(dir)
        return (pos[0]+dir[0], pos[1]+dir[1]), dir_list

    # Initialize pygame
    init()

    # create the grid
    grid = [[Pixel(i,j) for j in range(HEIGHT)] for i in range(WIDTH)]

    my_snake = deque()
    my_snake.append((randint(0, WIDTH), randint(HEIGHT)))
    grid[my_snake[0][0]][my_snake[0][1]].change_state(2)

    goal = (randint(0, WIDTH), randint(0, HEIGHT))
    while goal in my_snake:
        goal = (randint(0, WIDTH), randint(0, HEIGHT))
    grid[goal[0]][goal[1]].change_state(3)

    direction_list = [(1,0), (-1,0), (0,1), (0,-1)]
    temp_direction = direction_list[randint(0,4)]
    # guarantee that there is at least 5 moves prior to losing to begin with
    while not 0<=my_snake[0][0]+5*temp_direction[0]<WIDTH and 0<=my_snake[0][1]+5*temp_direction[1]<HEIGHT:
        temp_direction = direction_list[randint(0,4)]
    direction = temp_direction
    del temp_direction

    temp_direction_list = deque([])
    direction_list = deque([])
    og_pos = my_snake[0]
    if og_pos[0] == 0:
        if direction[1] == 1:
            if og_pos[1] > 1:
                og_pos, temp_direction_list = move_and_add(og_pos, (1,0), temp_direction_list)
                og_pos, temp_direction_list = move_and_add(og_pos, (0,-1), temp_direction_list)
                og_pos, temp_direction_list = move_and_add(og_pos, (-1,0), temp_direction_list)
            else:
                og_pos, temp_direction_list = move_and_add(og_pos, (1,0), temp_direction_list)
                og_pos, temp_direction_list = move_and_add(og_pos, (0,1), temp_direction_list)
                og_pos, temp_direction_list = move_and_add(og_pos, (-1,0), temp_direction_list)
    else:
        if direction[0] == 1:
            if og_pos[1] > 1:
                og_pos, temp_direction_list = move_and_add(og_pos, (0,1), temp_direction_list)
            else:
                og_pos, temp_direction_list = move_and_add(og_pos, (0,-1), temp_direction_list)
        while og_pos[0] != 0:
            og_pos, temp_direction_list = move_and_add(og_pos, (-1,0), temp_direction_list)


    # make a hamiltonian path under the assumption that WIDTH >= 2,
    # HEIGHT >= 4 and HEIGHT mod 2 == 0, otherwise there is either an error or
    # infinite loop while never obtaining the next goal
    pos = og_pos
    while pos[1] != 0:
        pos, direction_list = move_and_add(pos, (0,-1), direction_list)
    pos, direction_list = move_and_add(pos, (1,0), direction_list)
    for i in range(HEIGHT//2):
        for j in range(WIDTH - 2):
            pos, direction_list = move_and_add(pos, (1,0), direction_list)
        pos, direction_list = move_and_add(pos, (0,1), direction_list)
        for j in range(WIDTH - 2):
            pos, direction_list = move_and_add(pos, (-1,0), direction_list)
        if i != HEIGHT//2 - 1:
            pos, direction_list = move_and_add(pos, (0,1), direction_list)
    pos, direction_list = move_and_add(pos, (-1,0), direction_list)
    while pos[1] != og_pos[1]:
        pos, direction_list = move_and_add(pos, (0,-1), direction_list)






    running = True
    speed = 1
    while running:
        if len(my_snake) == WIDTH*HEIGHT:
            print('You won!')
            return

        if len(temp_direction_list) > 0:
            direction = temp_direction_list.popleft()
        else:
            direction = direction_list.popleft()
            direction_list.append(direction)

        events = get()
        for event in events:
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    return
                elif event.key == K_SPACE:
                    speed = (speed+1) % 5
            elif event.type == QUIT:
                return

        my_snake, goal, direction, grid, lost = advance(my_snake, goal, direction, grid)

        if lost:
            print("You lose!")
            print("Score: {}".format(len(my_snake)))
            return

        # draw what needs to be drawn
        screen.fill(BLACK)
        for i in range(WIDTH):
            for j in range(HEIGHT):
                grid[i][j].show()
        flip()

        if speed > 0:
            sleep((speed % 5)**-2 * hz)


if __name__ == '__main__':

    Snake()
