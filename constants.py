from pygame.display import set_mode

# Define constants
WIDTH = 30
HEIGHT = 20
GRID_PIXEL_SIZE = 15
SPACE_BETWEEN_PIXELS = 2
TOTAL = GRID_PIXEL_SIZE+SPACE_BETWEEN_PIXELS
SCREEN_WIDTH = WIDTH*TOTAL+1
SCREEN_HEIGHT = HEIGHT*TOTAL+1
MOVES = [[1,0], [0,1], [-1,0], [0,-1]]

# Define colors
WHITE = (255,255,255) # blank pixel
BLACK = (0,0,0) # background/wall
RED = (255,0,0) # apple/goal
BLUE = (0,0,255) # Snake
HEAD = (0,255,0) # Snake Head

# Dictionary from states to colors
state_dict = {0:WHITE, 1:BLUE, 2:HEAD, 3:RED}

# Initialize background
screen = set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
screen.fill(BLACK)

# Set hz (1/fps)
hz = 1/15
